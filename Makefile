default: test

test: test-unit test-pycodestyle test-mypy

test-unit:
	python3 testsuite.py

test-pycodestyle:
	pycodestyle maxmod.py

test-mypy:
	mypy --strict maxmod.py

clean:
	$(RM) -r __pycache__ .mypy_cache

.PHONY: default test test-unit test-pycodestyle test-mypy clean
